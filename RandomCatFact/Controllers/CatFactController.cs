﻿using Newtonsoft.Json;
using RandomCatFact.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace RandomCatFact.Controllers
{
    public class CatFactController : Controller
    {
        [HandleError]
        // GET: CatFact/Random
        public ActionResult Random()
        {
            GetRandomCatFact();
            CatFact fact = GetRandomCatFact();
            return View(fact);
        }
        protected CatFact GetRandomCatFact()
        {
            string json = "";
            WebRequest request = WebRequest.Create(ConfigurationManager.AppSettings["APIRandomCatFact"]);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            using (Stream dataStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(dataStream);
                json = reader.ReadToEnd();
            }
            response.Close();
            CatFact fact = JsonConvert.DeserializeObject<CatFact>(json);
            return fact;
        }
    }
}